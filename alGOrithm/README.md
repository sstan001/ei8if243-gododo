# Go

Projet d'Intelligence Artificielle, dans lequel nous devons implémenter un joueur de GO.

## Contributeurs

Équipe n°15

- Sophie STAN (sstan@enseirb-matmeca.fr)
- Antoine PRINGALLE (apringalle@enseirb-matmeca.fr)

## Environnement de travail

- Développement sur Pycharm
- dépôt GitLab public [EI8IF243-GOdodo](https://gitlab.com/sstan001/ei8if243-gododo) mis en miroir sur thor

## Organisation du projet

### Granularité du projet

Pour une gestion plus fine de l'implémentation de notre IA, ainsi que pour alléger l'API de `myPlayer.py`, un
fichier `ai.py` porte toute l'intelligence de la méthode `getPlayerMove`.

### Heuristique (class Heuristics)

Heuristique classique : Somme des différences "Nombre de pions sur le plateau", "Nombre de pions capturés"

### Alpha-beta avec mémoire (class AI)

Un attribut `self._my_hash = dict()` est rajouté à la classe `myPlayer`. Cet attribut est transmis retransmis à
Alpha-beta, qui stocke ses résultats de recherche des meilleurs coups dedans au fur et à mesure. Cette sauvegarde permet
d'aller plus vite après un premier appel à Alpha-beta.

#### Visualisation des branches d'alphabeta

Nous avons créé une classe Graphvizer qui permet de lancer l'algorithme alphabeta tout en traçant le graphe des
recherches. Les élagages Alpha-Beta sont observables sur le graphe par une couleur de noeud différente.

![](.README_images/graphe.png)

### Phases de jeu (class AI)

Le temps imparti de réflexion de notre IA : 8 minutes.

Les différents phases de jeu sont décrites en variables globales de la classe `AI` du fichier `ai.py`. On distingue les
4 phases de jeu suivantes :

- Première phase de jeu (~opening) :\
  `FIRST_PHASE = 1. / 3` correspond au pourcentage de remplissage du plateau de jeu. Durant la première phase, la
  fonction `opening_move` est appelée pour gérer les coups. Sa stratégie est basée sur l'aquisition d'yeux en jouant une
  case sur deux.


- Seconde phase de jeu (calcul long avec mémoire) :\
  `SECOND_PHASE = 1 * 60` correspond au temps jusqu'au quel la fonction `common_move` est appelée avec une profondeur
  égale à `DEPTH_2`.


- Troisieme phase de jeu :\
  `THIRD_PHASE = 5 * 60` correspond au temps jusqu'au quel la fonction `common_move` est appelée avec une profondeur
  égale à `DEPTH_3`.


- Quatrième phase de jeu :\
  `LAST_PHASE = 7 * 60` correspond au temps jusqu'au quel la fonction `common_move` est appelée avec une profondeur
  égale à `DEPTH_4`.


- Dernière phase :\
  dépassées les 7 minutes, `random_move` est appelée jusqu'à la fin.

## Commandes et résultats

- Lancer un match `myPlayer.py` VS `myPlayer.py` :

```shell 
python3 localGame.py
```

--> Le joueur noir est toujours gagnant

- Lancer un match entre `myPlayer` (NOIR) et `randomPlayer` (BLANC)

```shell 
python3 namedGame.py myPlayer randomPlayer
```

--> myPlayer gagne la majeure partie du temps

- Lancer un match entre `gnugoPlayer` (NOIR) et `myPlayer` (BLANC)

```shell 
python3 namedGame.py gnugoPlayer myPlayer
```

--> myPlayer est tout le temps perdant...

###