from random import choice


class Heuristics:
    @staticmethod
    def evaluate(board):
        return board.diff_stones_captured() + board.diff_stones_board()


class AI:
    INF = 10000000
    FIRST_PHASE = 1. / 3  # A percentage of the board

    DEPTH_2 = 2  # Depth used for alpha-beta before the beginning of the SECOND_PHASE
    SECOND_PHASE = 1 * 60  # Elapsed time (in seconds)

    DEPTH_3 = 1  # Depth used for alpha-beta before the beginning of the THIRD_PHASE
    THIRD_PHASE = 5 * 60  # Elapsed time (in seconds)

    DEPTH_4 = 1  # Depth used for alpha-beta before the beginning of the LAST_PHASE
    LAST_PHASE = 7 * 60  # Elapsed time (in seconds)

    @staticmethod
    def get_ai_move(board, totalTime, hashes):
        """ Handle the different types of move to play following the remaining time """
        nbPieces = board._nbWHITE + board._nbBLACK
        nbBoxes = board._BOARDSIZE * board._BOARDSIZE

        if board._lastPlayerHasPassed:
            my_score = board._nbBLACK if board._nextPlayer == board._BLACK else board._nbWHITE
            opponent_score = board._nbWHITE if board._nextPlayer == board._BLACK else board._nbBLACK
            if my_score > opponent_score:
                return -1  # Playing PASS
        if nbPieces <= nbBoxes * AI.FIRST_PHASE:
            print("\033[93m{}\033[0m".format("FIRST PHASE (opening moves)"))
            move = AI.opening_move(board)
        elif totalTime <= AI.SECOND_PHASE:
            print("\033[93m{}\033[0m".format("SECOND PHASE (depth=1)"))
            move = AI.common_move(board, AI.DEPTH_2, hashes)
        elif totalTime <= AI.THIRD_PHASE:
            print("\033[93m{}\033[0m".format("THIRD PHASE (depth=1)"))
            move = AI.common_move(board, AI.DEPTH_3, hashes)
        elif totalTime <= AI.LAST_PHASE:
            print("\033[93m{}\033[0m".format("LAST PHASE (depth=3)"))
            move = AI.common_move(board, AI.DEPTH_4, hashes)
        else:
            move = AI.random_move(board)
        return move

    @staticmethod
    def opening_move(board):
        """ Returns a move with a specific strategy: an odd move if it exists, else a random move """
        moves = board.legal_moves()
        for move in moves:
            if move % 2 == 1:
                return move
        move = choice(AI.filter_eyes(board))
        if move:
            return move
        else:
            return AI.random_move(board)

    @staticmethod
    def common_move(board, depth, hashes):
        """ Returns the best move found after evaluating the possibilities with alpha-beta """
        best_score = -AI.INF
        best_move = None

        moves = AI.filter_eyes(board)
        for move in moves:
            board.push(move)
            score = AI.alphabeta(board, depth, -AI.INF, AI.INF, hashes, True)
            board.pop()

            if best_score < score:
                best_score = score
                best_move = move
        return best_move

    @staticmethod
    def random_move(board):
        """ Returns a random move on a given board """
        return choice(board.legal_moves())

    @staticmethod
    def add_hash(hashes, hash, score):
        """ Add an entry to HASHES dict """
        hashes[hash] = score

    @staticmethod
    def get_score_from_hash(hashes, hash):
        """ Gets the score from an hash in a constant time. If hash does not exist, None is returned """
        try:
            return hashes[hash]
        except KeyError:
            return None

    @staticmethod
    def get_hash_from_board(board):
        """ Gets the hash for a board. Its value is computed by Goban.
        _currentHash is a protected variable but we can still access it """
        return board._currentHash

    @staticmethod
    def filter_eyes(board):
        """ Returns the legal moves of a board, after removing those which are in the centers of eyes """
        filtered_moves = list()
        legal_moves = board.legal_moves()
        for move in legal_moves:
            neighbors = board._get_neighbors(move)
            colors = [board[n] for n in neighbors]
            my_pawns = colors.count(board._nextPlayer)
            its_pawns = colors.count(board.flip(board._nextPlayer))
            empty = colors.count(0)
            # print(f"Move {move}: {my_pawns} pawns of my color, {its_pawns} pawns of its color, {empty} empty cells")

            if my_pawns > 3 or its_pawns > 3:
                pass  # We dont want to play here
            else:
                filtered_moves.append(move)
        # print("I'm going to play one of those moves:", moves)
        return filtered_moves

    @staticmethod
    def alphabeta(board, depth: int, alpha, beta, hashes, maximizing: bool):
        """ Alpha-beta pruning computes the heuristic of a move """
        my_hash = AI.get_hash_from_board(board)
        my_hash_score = AI.get_score_from_hash(hashes, my_hash)
        if my_hash_score is not None:
            print("\033[92mHASH HAS BEEN USED\033[0m")
            return my_hash_score

        if depth == 0 or board.is_game_over():
            score = Heuristics.evaluate(board)
            AI.add_hash(hashes, my_hash, score)  # adds a new value to our hash dict
            return score

        if maximizing:
            best_score = - AI.INF
            for move in AI.filter_eyes(board):
                board.push(move)
                score = AI.alphabeta(board, depth - 1, alpha, beta, hashes, maximizing=False)
                board.pop()

                best_score = max(best_score, score)
                alpha = max(alpha, score)

                if beta <= alpha:
                    break
            AI.add_hash(hashes, my_hash, best_score)
            return best_score

        else:
            best_score = AI.INF
            for move in AI.filter_eyes(board):
                board.push(move)
                score = AI.alphabeta(board, depth - 1, alpha, beta, hashes, maximizing=True)
                board.pop()

                best_score = min(best_score, score)
                beta = min(beta, best_score)

                if beta <= alpha:
                    break
            AI.add_hash(hashes, my_hash, best_score)
            return best_score
